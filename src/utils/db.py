import os
from motor.motor_asyncio import AsyncIOMotorClient

_connection = None


def get_connection():
    global _connection
    if not _connection:
        try:
            connection = AsyncIOMotorClient(os.getenv("MONGO_DB"))
            _connection = connection['message-counter-bot']
            print("Connected to MongoDB!")
        except Exception as error:
            print(f"Failed to connect to MongoDB! Error: {error}")
    return _connection
