from discord.ext import commands


class Ready(commands.Cog):
    @commands.Cog.listener()
    async def on_ready(self):
        print("Bot ready!")
