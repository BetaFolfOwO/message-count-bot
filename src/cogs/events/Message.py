import os
from utils import db
from discord.ext import commands


class Message(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        connection = db.get_connection()
        self.db = connection['counts']

    @commands.Cog.listener()
    async def on_message(self, message):
        if self.db != None:
            author = message.author
            userId = str(author.id)

            if author.bot is True:
                return

            try:
                await self.db.find_one_and_update({'_id': userId}, {'$inc': {'totalMessages': 1}}, upsert=True)
                if os.getenv("ENV") == "dev":
                    print(
                        f"Updated message count for {author.display_name}#{author.discriminator}")
            except Exception as error:
                print(
                    f"Failed to update message count for {author.display_name}#{author.discriminator}. Error: {error}")
